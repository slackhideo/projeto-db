<?php include "../models/q123.php" ?>

<script>
$("#lab").change(function() {
$.ajax({type:"POST", url: "views/q1.php", data:$("#formq1").serialize(),
success: $("#content").load("views/q1.php?" + $("#formq1").serialize())});
    return false;
});
$("#memb_nao_proj").click(function() {
    $("#content").load("views/q4.php");
});
</script>

<div class="center">
    <p class="title">Laboratório -> Membros</p>
    <form id="formq1" method="post">
    <select name="lab" id="lab">
        <option disabled selected>Selecione o laboratório</option>
<?php
while($row = $r123->fetch_assoc()) {
    echo "<option value='" . $row['idlaboratoriodepesquisa'] . "'>" .
    $row['nome'] . "</option>";
}
?>
    </select>
    </form>
 
    <p>OU</p>
    <p><a href="javascript: void(0);" id="memb_nao_proj">Consultar membros
    que não compõem a equipe do projeto "e-phenology"</a></p>

</div>

<?php
$con->close();
?>
