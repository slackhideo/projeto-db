<?php include "../models/q123.php" ?>

<script>
$("#lab").change(function() {
$.ajax({type:"POST", url: "views/q3.php", data:$("#formq3").serialize(),
success: $("#content").load("views/q3.php?" + $("#formq3").serialize())});
    return false;
});
$("#coord_proj").click(function() {
    $("#content").load("views/q5.php");
});
$("#inst_proj").click(function() {
    $("#content").load("views/q6.php");
});
$("#subproj").click(function() {
    $("#content").load("views/q9.php");
});
$("#contrib").click(function() {
    $("#content").load("views/q10.php");
});
$("#publ_conf").click(function() {
    $("#content").load("views/q19.php");
});
$("#add").click(function() {
    $("#content").load("forms/projadd.php");
});
</script>

<div class="center">
    <p class="title">Laboratório -> Projetos</p>
    <form id="formq3" method="post">
    <select name="lab" id="lab">
        <option disabled selected>Selecione o laboratório</option>
<?php
while($row = $r123->fetch_assoc()) {
    echo "<option value='" . $row['idlaboratoriodepesquisa'] . "'>" .
    $row['nome'] . "</option>";
}
?>
    </select>
    </form>

    <p>OU</p>
    <p><a href="javascript: void(0);" id="coord_proj">Consultar coordenadores
    dos projetos conduzidos entre abril de 2009 e julho de 2012</a></p>

    <p>OU</p>
    <p><a href="javascript: void(0);" id="inst_proj">Consultar a
    instituição que financia o projeto "e-phenology"</a></p>

    <p>OU</p>
    <p><a href="javascript: void(0);" id="subproj">Consultar os
    nomes dos subprojetos do projeto "e-phenology"</a></p>

    <p>OU</p>
    <p><a href="javascript: void(0);" id="contrib">Consultar as
    contribuições associadas ao projeto "e-phenology"</a></p>

    <p>OU</p>
    <p><a href="javascript: void(0);" id="publ_conf">Consultar qual
    projeto gerou mais publicações em conferências</a></p>

    <p>OU</p>
    <p><a href="javascript: void(0);" id="add">Incluir novo projeto</a></p>

</div>

<?php
$con->close();
?>
