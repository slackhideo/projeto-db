<?php include "../models/q123.php" ?>

<script>
$("#lab").change(function() {
$.ajax({type:"POST", url: "views/q2.php", data:$("#formq2").serialize(),
success: $("#content").load("views/q2.php?" + $("#formq2").serialize())});
    return false;
});
</script>

<div class="center">
    <p class="title">Laboratório -> Professores colaboradores</p>
    <form id="formq2" method="post">
    <select name="lab" id="lab">
        <option disabled selected>Selecione o laboratório</option>
<?php
while($row = $r123->fetch_assoc()) {
    echo "<option value='" . $row['idlaboratoriodepesquisa'] . "'>" .
    $row['nome'] . "</option>";
}
?>
    </select>
    </form>
</div>

<?php
$con->close();
?>
