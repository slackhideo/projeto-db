<?php
/* models/con.php */

$con = new mysqli("localhost", "root", "123", "mydb");
if($con->connect_errno) {
    echo "Um problema ocorreu ao tentar conectar ao banco de dados:<br /> "
    . $con->connect_error;
    exit();
}

$con->query("SET NAMES utf8");
$con->query("SET CHARACTER SET utf8");
$con->set_charset("utf8");
?>
