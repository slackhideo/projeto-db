<?php
/* models/q18.php */

include "con.php";

$q18 = "SELECT descricao FROM Contribuicao,
(SELECT pesqdesproj.Pesquisador_idpesquisador AS id FROM 
Projeto AS proj, 
Pesquisador_desenvolve_Projeto AS pesqdesproj
WHERE 
proj.LaboratorioDePesquisa_idlaboratoriodepesquisa = 1 AND 
pesqdesproj.Projeto_numero = proj.numero) AS plab, AlunoDeDoutorado AS doc,
Pesquisador_cria_Contribuicao AS pesqcria
WHERE
doc.Aluno_Pesquisador_idpesquisador = plab.id AND
pesqcria.Pesquisador_idpesquisador = plab.id AND
pesqcria.Contribuicao_idcontribuicao = Contribuicao.idcontribuicao";

$r18 = $con->query($q18);

?>
