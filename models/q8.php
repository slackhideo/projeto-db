<?php
/* models/q8.php */

include "con.php";

$q8 = "SELECT pesqdef.nome FROM Orientador_orienta_Aluno AS ori, Tese AS t, Pesquisador AS pesq, Pesquisador AS pesqdef
WHERE
ori.Orientador_Pesquisador_idpesquisador = pesq.idpesquisador AND
pesq.nome = 'Anderson Rocha' AND
(
t.AlunoDeDoutorado_Aluno_Pesquisador_idpesquisador = ori.Aluno_Pesquisador_idpesquisador
OR
t.AlunoDeMestrado_Aluno_Pesquisador_idpesquisador = ori.Aluno_Pesquisador_idpesquisador
) AND
date(t.datadefesa) >= date('2012-01-01') AND
date(t.datadefesa) <= date('2012-12-31') AND
ori.Aluno_Pesquisador_idpesquisador = pesqdef.idpesquisador";

$r8 = $con->query($q8);

?>
