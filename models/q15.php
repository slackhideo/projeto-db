<?php
/* models/q15.php */

include "con.php";

$q15 = "SELECT COUNT(b.idbanca) as num
FROM Banca AS b, Pesquisador AS p, Orientador_forma_Banca AS f, Tese AS t, Pesquisador_desenvolve_Projeto AS d, Projeto AS j
WHERE p.nome = 'Helio Pedrini'
AND f.Orientador_Pesquisador_idpesquisador = p.idPesquisador
AND f.Banca_idbanca = b.idbanca
AND b.Tese_Contribuicao_idcontribuicao = t.Contribuicao_idcontribuicao
AND t.AlunoDeDoutorado_Aluno_Pesquisador_idpesquisador = d.Pesquisador_idPesquisador
AND d.Projeto_numero = j.numero
AND j.LaboratorioDePesquisa_idlaboratoriodepesquisa = 1";

$r15 = $con->query($q15);

?>
