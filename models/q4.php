<?php
/* models/q4.php */

include "con.php";

$q4 = "SELECT nome FROM Pesquisador,
(
SELECT pesqdesproj.Pesquisador_idpesquisador AS id FROM 
Projeto AS proj, 
Pesquisador_desenvolve_Projeto AS pesqdesproj
WHERE
proj.LaboratorioDePesquisa_idlaboratoriodepesquisa = 1 AND 
pesqdesproj.Projeto_numero = proj.numero 
) AS q1
LEFT JOIN
(
SELECT pesqdesproj.Pesquisador_idpesquisador AS id FROM 
Projeto AS proj, 
Pesquisador_desenvolve_Projeto AS pesqdesproj
WHERE
proj.LaboratorioDePesquisa_idlaboratoriodepesquisa = 1 AND 
pesqdesproj.Projeto_numero = proj.numero AND
proj.numero = 1
) AS q2
ON
q1.id = q2.id
WHERE q2.id IS NULL
AND q1.id = Pesquisador.idpesquisador";

$r4 = $con->query($q4);

?>
