<?php include "../models/q7.php" ?>
<?php

if($r7->num_rows < 1) {
    echo "<p class='center'>Sua consulta não retornou nenhum resultado</p>";
    exit();
}

elseif($r7->num_rows == 1) {
    echo "<p>Sua consulta retornou 1 resultado:</p>";
}

else {
    echo "<p>Sua consulta retornou " .  $r7->num_rows . " resultados:</p>";
}
?>

<table class="result">
<tr>
    <th>Orientador do mestrado de Jurandy Almeida</th>
</tr>
<?php
while($row = $r7->fetch_assoc()) {
?>
<tr>
    <td><?php echo $row['nome'] ?></td>
</tr>
<?php
}
?>
</table>

<?php
$con->close();
?>
