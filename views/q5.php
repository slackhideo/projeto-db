<?php include "../models/q5.php" ?>
<?php

if($r5->num_rows < 1) {
    echo "<p class='center'>Sua consulta não retornou nenhum resultado</p>";
    exit();
}

elseif($r5->num_rows == 1) {
    echo "<p>Sua consulta retornou 1 resultado:</p>";
}

else {
    echo "<p>Sua consulta retornou " .  $r5->num_rows . " resultados:</p>";
}
?>

<table class="result">
<tr>
    <th>Coordenadores dos projetos conduzidos entre
    abril de 2009 e julho de 2012</th>
</tr>
<?php
while($row = $r5->fetch_assoc()) {
?>
<tr>
    <td><?php echo $row['nome'] ?></td>
</tr>
<?php
}
?>
</table>

<?php
$con->close();
?>
