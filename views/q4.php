<?php include "../models/q4.php" ?>
<?php

if($r4->num_rows < 1) {
    echo "<p class='center'>Sua consulta não retornou nenhum resultado</p>";
    exit();
}

elseif($r4->num_rows == 1) {
    echo "<p>Sua consulta retornou 1 resultado:</p>";
}

else {
    echo "<p>Sua consulta retornou " .  $r4->num_rows . " resultados:</p>";
}
?>

<table class="result">
<tr>
    <th>Membros que não compõe a equipe do projeto "e-phenology"</th>
</tr>
<?php
while($row = $r4->fetch_assoc()) {
?>
<tr>
    <td><?php echo $row['nome'] ?></td>
</tr>
<?php
}
?>
</table>

<?php
$con->close();
?>
