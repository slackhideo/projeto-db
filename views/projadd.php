<?php include "../models/q19.php" ?>
<?php

if($r19->num_rows < 1) {
    echo "<p class='center'>Sua consulta não retornou nenhum resultado</p>";
    exit();
}

elseif($r19->num_rows == 1) {
    echo "<p>Sua consulta retornou 1 resultado:</p>";
}

else {
    echo "<p>Sua consulta retornou " . $r19->num_rows . " resultados:</p>";
}
?>

<table class="result">
<tr>
    <th>Projeto</th>
    <th>Artigos</th>
</tr>
<?php
while($row = $r19->fetch_assoc()) {
?>
<tr>
    <td><?php echo $row['titulo'] ?></td>
    <td><?php echo $row['descricao'] ?></td>
</tr>
<?php
}
?>
</table>

<?php
$con->close();
?>
