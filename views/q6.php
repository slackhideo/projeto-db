<?php include "../models/q6.php" ?>
<?php

if($r6->num_rows < 1) {
    echo "<p class='center'>Sua consulta não retornou nenhum resultado</p>";
    exit();
}

elseif($r6->num_rows == 1) {
    echo "<p>Sua consulta retornou 1 resultado:</p>";
}

else {
    echo "<p>Sua consulta retornou " .  $r6->num_rows . " resultados:</p>";
}
?>

<table class="result">
<tr>
    <th>Instituição que financia o projeto "e-phenology"</th>
</tr>
<?php
while($row = $r6->fetch_assoc()) {
?>
<tr>
    <td><?php echo $row['nome'] ?></td>
</tr>
<?php
}
?>
</table>

<?php
$con->close();
?>
