<?php include "../models/q12.php" ?>
<?php

if($r12->num_rows < 1) {
    echo "<p class='center'>Sua consulta não retornou nenhum resultado</p>";
    exit();
}

elseif($r12->num_rows == 1) {
    echo "<p>Sua consulta retornou 1 resultado:</p>";
}

else {
    echo "<p>Sua consulta retornou " . $r12->num_rows . " resultados:</p>";
}
?>

<table class="result">
<tr>
    <th>Artigos publicados pelo Prof. Anderson Rocha em 2011</th>
</tr>
<?php
while($row = $r12->fetch_assoc()) {
?>
<tr>
    <td><?php echo $row['titulo'] ?></td>
</tr>
<?php
}
?>
</table>

<?php
$con->close();
?>
