<?php include "../models/q17.php" ?>
<?php

if($r17->num_rows < 1) {
    echo "<p class='center'>Sua consulta não retornou nenhum resultado</p>";
    exit();
}

elseif($r17->num_rows == 1) {
    echo "<p>Sua consulta retornou 1 resultado:</p>";
}

else {
    echo "<p>Sua consulta retornou " . $r17->num_rows . " resultados:</p>";
}
?>

<table class="result">
<tr>
    <th>Data em que a palestra <em>Reasoning for Complex
    Data</em> foi apresentada</th>
</tr>
<?php
while($row = $r17->fetch_assoc()) {
?>
<tr>
    <td><?php echo $row['data'] ?></td>
</tr>
<?php
}
?>
</table>

<?php
$con->close();
?>
