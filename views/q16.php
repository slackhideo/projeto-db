<?php include "../models/q16.php" ?>
<?php

if($r16->num_rows < 1) {
    echo "<p class='center'>Sua consulta não retornou nenhum resultado</p>";
    exit();
}

elseif($r16->num_rows == 1) {
    echo "<p>Sua consulta retornou 1 resultado:</p>";
}

else {
    echo "<p>Sua consulta retornou " . $r16->num_rows . " resultados:</p>";
}
?>

<table class="result">
<tr>
    <th>Patentes registradas pelo Prof. Anderson Rocha</th>
</tr>
<?php
while($row = $r16->fetch_assoc()) {
?>
<tr>
    <td><?php echo $row['titulo'] ?></td>
</tr>
<?php
}
?>
</table>

<?php
$con->close();
?>
