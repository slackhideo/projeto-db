<?php include "../models/q8.php" ?>
<?php

if($r8->num_rows < 1) {
    echo "<p class='center'>Sua consulta não retornou nenhum resultado</p>";
    exit();
}

elseif($r8->num_rows == 1) {
    echo "<p>Sua consulta retornou 1 resultado:</p>";
}

else {
    echo "<p>Sua consulta retornou " .  $r8->num_rows . " resultados:</p>";
}
?>

<table class="result">
<tr>
    <th>Alunos orientados pelo prof. Anderson Rocha
    que defenderam em 2012</th>
</tr>
<?php
while($row = $r8->fetch_assoc()) {
?>
<tr>
    <td><?php echo $row['nome'] ?></td>
</tr>
<?php
}
?>
</table>

<?php
$con->close();
?>
