<?php include "../models/q123.php" ?>
<?php

$q = "SELECT p.nome
FROM Pesquisador AS p, Pesquisador_desenvolve_Projeto AS d, Projeto AS j
WHERE p.idPesquisador = d.Pesquisador_idPesquisador
AND j.numero = d.Projeto_numero
AND j.LaboratorioDePesquisa_idlaboratoriodepesquisa = " . $_GET["lab"];

$r1 = $con->query($q);

if($r1->num_rows < 1) {
    echo "<p class='center'>Sua consulta não retornou nenhum resultado</p>";
    exit();
}

elseif($r1->num_rows == 1) {
    echo "<p>Sua consulta retornou 1 resultado:</p>";
}

else {
    echo "<p>Sua consulta retornou " .  $r1->num_rows . " resultados:</p>";
}
?>

<table class="result">
<tr>
    <th>Membros do laboratório</th>
</tr>
<?php
while($row = $r1->fetch_assoc()) {
?>
<tr>
    <td><?php echo $row['nome'] ?></td>
</tr>
<?php
}
?>
</table>

<?php
$con->close();
?>
