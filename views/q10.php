<?php include "../models/q10.php" ?>
<?php

if($r10->num_rows < 1) {
    echo "<p class='center'>Sua consulta não retornou nenhum resultado</p>";
    exit();
}

elseif($r10->num_rows == 1) {
    echo "<p>Sua consulta retornou 1 resultado:</p>";
}

else {
    echo "<p>Sua consulta retornou " . $r10->num_rows . " resultados:</p>";
}
?>

<table class="result">
<tr>
    <th>Contribuições associadas ao projeto "e-phenology"</th>
</tr>
<?php
while($row = $r10->fetch_assoc()) {
?>
<tr>
    <td><?php echo $row['descricao'] ?></td>
</tr>
<?php
}
?>
</table>

<?php
$con->close();
?>
