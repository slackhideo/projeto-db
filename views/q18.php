<?php include "../models/q18.php" ?>
<?php

if($r18->num_rows < 1) {
    echo "<p class='center'>Sua consulta não retornou nenhum resultado</p>";
    exit();
}

elseif($r18->num_rows == 1) {
    echo "<p>Sua consulta retornou 1 resultado:</p>";
}

else {
    echo "<p>Sua consulta retornou " . $r18->num_rows . " resultados:</p>";
}
?>

<table class="result">
<tr>
    <th>Publicações associadas a alunos de doutorado do laboratório</th>
</tr>
<?php
while($row = $r18->fetch_assoc()) {
?>
<tr>
    <td><?php echo $row['descricao'] ?></td>
</tr>
<?php
}
?>
</table>

<?php
$con->close();
?>
