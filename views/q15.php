<?php include "../models/q15.php" ?>
<?php

if($r15->num_rows < 1) {
    echo "<p class='center'>Sua consulta não retornou nenhum resultado</p>";
    exit();
}

elseif($r15->num_rows == 1) {
    echo "<p>Sua consulta retornou 1 resultado:</p>";
}

else {
    echo "<p>Sua consulta retornou " . $r15->num_rows . " resultados:</p>";
}
?>

<table class="result">
<tr>
    <th>Número de vezes que o Prof. Hélio Pedrini participou de
    bancas de doutorado de alunos do laboratório</th>
</tr>
<?php
while($row = $r15->fetch_assoc()) {
?>
<tr>
    <td><?php echo $row['num'] ?></td>
</tr>
<?php
}
?>
</table>

<?php
$con->close();
?>
