<?php include "../models/q9.php" ?>
<?php

if($r9->num_rows < 1) {
    echo "<p class='center'>Sua consulta não retornou nenhum resultado</p>";
    exit();
}

elseif($r9->num_rows == 1) {
    echo "<p>Sua consulta retornou 1 resultado:</p>";
}

else {
    echo "<p>Sua consulta retornou " . $r9->num_rows . " resultados:</p>";
}
?>

<table class="result">
<tr>
    <th>Subprojetos do projeto "e-phenology"</th>
</tr>
<?php
while($row = $r9->fetch_assoc()) {
?>
<tr>
    <td><?php echo $row['titulo'] ?></td>
</tr>
<?php
}
?>
</table>

<?php
$con->close();
?>
