<?php include "../models/q123.php" ?>
<?php

$q = "SELECT distinct nome FROM PesquisadorColaborador as pesqcolab, Pesquisador_desenvolve_Projeto as pesqdesenvproj, Projeto, Pesquisador
WHERE 
pesqcolab.Orientador_Pesquisador_idpesquisador = pesqdesenvproj.Pesquisador_idpesquisador and
pesqdesenvproj.Projeto_numero = Projeto.numero and
Pesquisador.idpesquisador = pesqdesenvproj.Pesquisador_idpesquisador and
Projeto.LaboratorioDePesquisa_idlaboratoriodepesquisa = " . $_GET["lab"];

$r2 = $con->query($q);

if($r2->num_rows < 1) {
    echo "<p class='center'>Sua consulta não retornou nenhum resultado</p>";
    exit();
}

elseif($r2->num_rows == 1) {
    echo "<p>Sua consulta retornou 1 resultado:</p>";
}

else {
    echo "<p>Sua consulta retornou " .  $r2->num_rows . " resultados:</p>";
}
?>

<table class="result">
<tr>
    <th>Professores colaboradores do laboratório</th>
</tr>
<?php
while($row = $r2->fetch_assoc()) {
?>
<tr>
    <td><?php echo $row['nome'] ?></td>
</tr>
<?php
}
?>
</table>

<?php
$con->close();
?>
