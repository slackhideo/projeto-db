<?php include "../models/q123.php" ?>
<?php

$q = "SELECT *
FROM Projeto AS p
WHERE p.dtFim is null AND p.dtinicio is not null AND p.LaboratorioDePesquisa_idlaboratoriodepesquisa = " . $_GET["lab"];

$r3 = $con->query($q);

if($r3->num_rows < 1) {
    echo "<p class='center'>Sua consulta não retornou nenhum resultado</p>";
    exit();
}

elseif($r3->num_rows == 1) {
    echo "<p>Sua consulta retornou 1 resultado:</p>";
}

else {
    echo "<p>Sua consulta retornou " .  $r3->num_rows . " resultados:</p>";
}
?>

<table class="result">
<tr>
    <th>Projetos em execução no laboratório</th>
</tr>
<?php
while($row = $r3->fetch_assoc()) {
?>
<tr>
    <td><?php echo $row['titulo'] ?></td>
</tr>
<?php
}
?>
</table>

<?php
$con->close();
?>
