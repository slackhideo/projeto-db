<?php include "../models/q11.php" ?>
<?php

if($r11->num_rows < 1) {
    echo "<p class='center'>Sua consulta não retornou nenhum resultado</p>";
    exit();
}

elseif($r11->num_rows == 1) {
    echo "<p>Sua consulta retornou 1 resultado:</p>";
}

else {
    echo "<p>Sua consulta retornou " . $r11->num_rows . " resultados:</p>";
}
?>

<table class="result">
<tr>
    <th>Artigos com classificação Qualis A1 publicados em 2012 em
    periódicos internacionais</th>
</tr>
<?php
while($row = $r11->fetch_assoc()) {
?>
<tr>
    <td><?php echo $row['titulo'] ?></td>
</tr>
<?php
}
?>
</table>

<?php
$con->close();
?>
