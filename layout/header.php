<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-BR" xml:lang="pt-BR">
<head>
    <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
    <title>Projeto de Sistema de Gerenciamento de Produção
    Científica</title>
    <link href="stylesheets/styles.css" rel="stylesheet" />
    <script src="javascripts/jquery.js" type="text/javascript"></script>
    <script src="javascripts/scripts.js" type="text/javascript"></script>
</head>
<body>

<div class="menu">
    <a href="javascript:void(0)" class="menu_link" id="menu_lab">Laboratório</a>
    <div id="menu_lab_box">
        <a href="javascript:void(0)" class="menu_link" id="menu_memb">Membros</a>
        <a href="javascript:void(0)" class="menu_link" id="menu_prof_colab">Prof. colaboradores</a>
        <a href="javascript:void(0)" class="menu_link" id="menu_proj">Projetos</a>
    </div>
    <a href="javascript:void(0)" class="menu_link" id="menu_prof">Professores</a>
    <a href="javascript:void(0)"
    class="menu_link">Estágios sanduíche</a>
    <a href="javascript:void(0)" class="menu_link" id="menu_dout">Doutorados</a>
    <a href="javascript:void(0)" class="menu_link" id="menu_mestr">Mestrados</a>
    <a href="javascript:void(0)" class="menu_link" id="menu_def">Defesas</a>
    <a href="javascript:void(0)" class="menu_link" id="menu_banca">Bancas</a>
    <a href="javascript:void(0)" class="menu_link" id="menu_art">Artigos</a>
    <a href="javascript:void(0)" class="menu_link" id="menu_pale">Palestras</a>
    <a href="javascript:void(0)" class="menu_link" id="menu_pat">Patentes</a>
</div>

<div class="content" id="content">
