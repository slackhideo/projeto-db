$(document).ready(function() {

    $('#menu_lab_box').hide();
    $('#menu_lab').click(function() {
	$('#menu_lab_box').toggle('slow');
	return false;
    });

    $("#menu_memb").click(function() {
        $("#content").load("forms/q1.php");
    });

    $("#menu_prof_colab").click(function() {
        $("#content").load("forms/q2.php");
    });

    $("#menu_proj").click(function() {
        $("#content").load("forms/q3.php");
    });

    $("#menu_mestr").click(function() {
        $("#content").load("forms/q7.php");
    });

    $("#menu_def").click(function() {
        $("#content").load("forms/q8.php");
    });

    $("#menu_art").click(function() {
        $("#content").load("forms/q11.php");
    });

    $("#menu_banca").click(function() {
        $("#content").load("forms/q15.php");
    });

    $("#menu_pat").click(function() {
        $("#content").load("forms/q16.php");
    });

    $("#menu_pale").click(function() {
        $("#content").load("forms/q17.php");
    });

    $("#menu_dout").click(function() {
        $("#content").load("forms/q18.php");
    });

});

function access(path){
    $("#content").load(path);
    return;
}

function q1form() {
    $.ajax({type:"POST", url: "views/q1.php", data:$("#formq1").serialize(),
success: $("#content").load("views/q1.php?" + $("#formq1").serialize())});
    return false;
}

